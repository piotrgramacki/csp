package variables;

import constraints.Constraint;

import java.util.*;
import java.util.stream.Collectors;

public class Variable {
    private String id;
    private Integer value;
    private List<Integer> wholeDomain;
    private List<Integer> availableDomain;
    private Set<Constraint> constraints;
    private Stack<List<Integer>> domainHistory;
    private boolean isFixed = false;

    public Variable(String id, Collection<Integer> wholeDomain) {
        this.wholeDomain = new LinkedList<>(wholeDomain);
        this.availableDomain = new LinkedList<>(wholeDomain);
        this.constraints = new HashSet<>();
        this.domainHistory = new Stack<>();
        this.id = id;
    }

    public int getWholeDomainSize() {
        return wholeDomain.size();
    }

    public int getAvailableDomainSize() {
        return availableDomain.size();
    }

    public void addConstraint(Constraint constraint) {
        constraints.add(constraint);
    }

    public int getConstraintsNumber() {
        return constraints.size();
    }

    public Integer getValue() {
        return value;
    }

    public void setFixed(Integer value) {
        this.value = value;
        this.isFixed = true;
        availableDomain.clear();
    }

    public boolean isSet() {
        return value != null;
    }

    public boolean isNull() {
        return value == null;
    }

    public boolean isNullAndDifferent(Variable other) {
        return isNull() && this != other;
    }

    public boolean check() {
        return constraints.stream().allMatch(Constraint::check);
    }

    public boolean nextValue() {
        if (availableDomain.isEmpty()) {
            return false;
        } else {
            value = getNextValue();
            return true;
        }
    }

    public void reset() {
        if (!isFixed) {
            value = null;
            availableDomain.clear();
            if (domainHistory.empty()) {
                availableDomain.addAll(wholeDomain);
            } else {
                availableDomain.addAll(domainHistory.pop());
            }
        }
    }

    public boolean hasNextValue() {
        return !availableDomain.isEmpty();
    }

    public void sortDomain() {
        List<Variable> constrainedVariables = getConnectedVariables();
        Map<Integer, Integer> availableValuesLeft = new HashMap<>();

        Integer prevValue = value;

        for (Integer possibility : availableDomain) {
            value = possibility;

            constrainedVariables.forEach(Variable::cutDomain);

            int availableInConstrained = constrainedVariables.stream()
                    .mapToInt(Variable::getAvailableDomainSize).sum();

            availableValuesLeft.put(possibility, availableInConstrained);

            constrainedVariables.forEach(Variable::restoreDomain);
        }

        availableDomain.sort((o1, o2) -> availableValuesLeft.get(o2) - availableValuesLeft.get(o1));

        value = prevValue;
    }

    public List<Variable> getConnectedVariables() {
        return constraints.stream()
                .flatMap(constraint -> constraint.getVariables().stream())
                .distinct()
                .filter(variable -> variable.isNullAndDifferent(this))
                .collect(Collectors.toList());
    }

    public void cutDomain() {
        addToHistory();
        List<Integer> newDomain = new LinkedList<>();

        for (int i = 0; i < availableDomain.size(); i++) {
            value = availableDomain.get(i);
            if (check()) {
                newDomain.add(availableDomain.get(i));
            }
            if (!isFixed) {
                value = null;
            }
        }

        availableDomain = newDomain;
    }

    public void restoreDomain() {
        availableDomain = domainHistory.pop();
    }

    public void storeDomain() {
        addToHistory();
    }

    public void removeFromDomain(List<Integer> values) {
        availableDomain.removeAll(values);
    }

    public String getId() {
        return id;
    }

    //////////////////////////////////////////////////
    private Integer getNextValue() {
        return availableDomain.remove(0);
    }

    private void addToHistory() {
        domainHistory.push(availableDomain);
        availableDomain = new LinkedList<>(availableDomain);
    }
}
