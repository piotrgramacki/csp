package constraints;

import variables.Variable;

import java.util.List;
import java.util.stream.Collectors;

public class SkyscrapperConstraint implements Constraint {
    private List<Variable> variables;
    private int numberToSee;

    public SkyscrapperConstraint(int numberToSee, List<Variable> variables) {
        this.numberToSee = numberToSee;
        this.variables = variables;
    }

    @Override
    public boolean check() {
        int seen = 0;
        int tallest = 0;

        List<Variable> setVars = variables.stream().filter(Variable::isSet).collect(Collectors.toList());
        List<Variable> notSetVars = variables.stream().filter(variable -> !variable.isSet()).collect(Collectors.toList());

        for (Variable variable : setVars) {
            if (variable.isSet() && variable.getValue() > tallest) {
                seen++;
                tallest = variable.getValue();
            }
        }

        // if all set - check directly
        if (notSetVars.size() == 0)
            return seen == numberToSee;

        // check if it is possible to see required number of towers
        // adding one building can increase number of seen blocks by max 1
        return numberToSee - seen <= notSetVars.size();

        // TODO - check if this works correct
    }

    @Override
    public List<Variable> getVariables() {
        return variables;
    }
}
