package constraints;

import variables.Variable;

import java.util.List;

public interface Constraint {
    boolean check();

    List<Variable> getVariables();
}
