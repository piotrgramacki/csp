package constraints;

import variables.Variable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BoardConstraint implements Constraint{
    private Set<Variable> variables;

    public BoardConstraint(Set<Variable> variables) {
        this.variables = variables;
    }

    @Override
    public boolean check() {
//        List<Integer> values = variables.stream().filter(Variable::isSet).map(Variable::getValue).collect(Collectors.toList());
//
//        return values.size() ==values.stream().distinct().collect(Collectors.toList()).size();

//        return variables.stream()
//                .filter(Variable::isSet)
//                .map(Variable::getValue)
//                .count() ==
//                variables.stream()
//                        .filter(Variable::isSet)
//                        .map(Variable::getValue)
//                        .distinct()
//                        .count();

        HashSet<Integer> distinctValues = new HashSet<>();
        int allSetCount = 0;
        int distinctSetCount = 0;

        for (Variable v :
                variables) {
            if (v.isSet()) {
                allSetCount++;
                if (distinctValues.add(v.getValue())) {
                    distinctSetCount++;
                }
            }
        }

        return allSetCount == distinctSetCount;
    }

    @Override
    public List<Variable> getVariables() {
        return new ArrayList<>(variables);
    }
}
