package constraints;

import variables.Variable;

import java.util.ArrayList;
import java.util.List;

public class FutoshikiConstraint implements Constraint {
    private Variable smaller;
    private Variable bigger;
    private List<Variable> variables;
    private int maxValue;
    private int minValue;

    public FutoshikiConstraint(Variable smaller, Variable bigger) {
        this.smaller = smaller;
        this.bigger = bigger;

        minValue = 1;
        maxValue = smaller.getWholeDomainSize();

        variables = new ArrayList<>();
        variables.add(smaller);
        variables.add(bigger);
    }

    @Override
    public boolean check() {
        if (smaller.isSet() && smaller.getValue() == maxValue)
            return false;

        if (bigger.isSet() && bigger.getValue() == minValue)
            return false;

        return !smaller.isSet() || !bigger.isSet() || smaller.getValue() < bigger.getValue();
    }

    @Override
    public List<Variable> getVariables() {
        return variables;
    }
}
