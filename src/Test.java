import problems.FutoshikiProblem;
import problems.Problem;
import problems.SkyscrapperProblem;
import solvers.BacktrackSolver;
import solvers.ForwardCheckSolver;
import solvers.Solver;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

/**
 * Contains example usage of CSPSolver
 */
public class Test {
    public static void main(String[] args) {
        Problem futoshikiProblem = new FutoshikiProblem("test_futo_8_2");
//        Problem skyscrapperProblem = new SkyscrapperProblem("test_sky_5_1");
//
        Solver forwardCheckSolver = new ForwardCheckSolver();
        Solver backtrackSolver = new BacktrackSolver();
//
//        runTest(futoshikiProblem, backtrackSolver);
//        runTest(futoshikiProblem, forwardCheckSolver);

//        runFutoTest(backtrackSolver);
        runSkyTests(backtrackSolver);
    }

    private static void runFutoTest(Solver solver) {
        Problem[] problems = new Problem[]{
                new FutoshikiProblem("test_futo_4_0"),
                new FutoshikiProblem("test_futo_4_1"),
                new FutoshikiProblem("test_futo_4_2"),
                new FutoshikiProblem("test_futo_5_0"),
                new FutoshikiProblem("test_futo_5_1"),
                new FutoshikiProblem("test_futo_5_2"),
                new FutoshikiProblem("test_futo_6_0"),
                new FutoshikiProblem("test_futo_6_1"),
                new FutoshikiProblem("test_futo_6_2"),
//                new FutoshikiProblem("test_futo_7_0"),
//                new FutoshikiProblem("test_futo_7_1"),
//                new FutoshikiProblem("test_futo_7_2"),
//                new FutoshikiProblem("test_futo_8_0"),
//                new FutoshikiProblem("test_futo_8_1"),
//                new FutoshikiProblem("test_futo_8_2"),
//                new FutoshikiProblem("test_futo_9_0"),
//                new FutoshikiProblem("test_futo_9_1"),
//                new FutoshikiProblem("test_futo_9_2")
        };

        try (PrintWriter pw = new PrintWriter("BT_futo_tests_all_heuristics.txt")) {
            pw.println("BT - all heuristics");

            ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

            runProblems(solver, problems, pw, threadMXBean);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    private static void runSkyTests(Solver solver) {
        Problem[] problems = new Problem[]{
//                new SkyscrapperProblem("test_sky_4_0"),
//                new SkyscrapperProblem("test_sky_4_1"),
//                new SkyscrapperProblem("test_sky_4_2"),
//                new SkyscrapperProblem("test_sky_4_3"),
//                new SkyscrapperProblem("test_sky_4_4"),
//                new SkyscrapperProblem("test_sky_5_0"),
//                new SkyscrapperProblem("test_sky_5_1"),
//                new SkyscrapperProblem("test_sky_5_2"),
//                new SkyscrapperProblem("test_sky_5_3"),
//                new SkyscrapperProblem("test_sky_5_4"),
                new SkyscrapperProblem("test_sky_6_0"),
                new SkyscrapperProblem("test_sky_6_1"),
                new SkyscrapperProblem("test_sky_6_2"),
//                new SkyscrapperProblem("test_sky_6_3"),
                new SkyscrapperProblem("test_sky_6_4"),
        };

        try (PrintWriter pw = new PrintWriter("BT_sky_tests_no_heuristics_2.txt")) {
            pw.println("BT - no heuristics");

            // used for accurate time measurements
            ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();

            runProblems(solver, problems, pw, threadMXBean);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void runProblems(Solver solver, Problem[] problems, PrintWriter pw, ThreadMXBean threadMXBean) {
        for (Problem p :
                problems) {
            p.load();

            long start = threadMXBean.getCurrentThreadCpuTime();
            int checks = solver.solve(p);
            int first = solver.getCheckedToFirst();
            long finish = threadMXBean.getCurrentThreadCpuTime();

            long durationSeconds = (finish - start) / 1000000000;
            long durationMillis = (finish - start) / 1000000;

            pw.printf("%s\nExecution time = %2d:%2d:%3d\nChecked %d values\nTo first solution: %d values\n\n", p.getName(), durationSeconds / 60, durationSeconds % 60, durationMillis % 1000, checks, first);
            System.out.printf("Execution time = %2d:%2d:%3d\nChecked %d values\nTo first solution: %d values\n\n", durationSeconds / 60, durationSeconds % 60, durationMillis % 1000, checks, first);
        }
    }

    private static void runTest(Problem problem, Solver solver) {
        problem.load();

        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        long start = threadMXBean.getCurrentThreadCpuTime();
        int checks = solver.solve(problem);
        int first = solver.getCheckedToFirst();
        long finish = threadMXBean.getCurrentThreadCpuTime();

        long durationSeconds = (finish - start) / 1000000000;

        System.out.printf("Execution time = %2d:%2d\nChecked %d values\nTo first solution: %d values\n\n", durationSeconds / 60, durationSeconds % 60, checks, first);
    }
}
