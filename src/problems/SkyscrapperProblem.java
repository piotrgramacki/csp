package problems;

import constraints.Constraint;
import constraints.SkyscrapperConstraint;
import variables.Variable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class SkyscrapperProblem extends BoardProblem {
    private boolean isLoaded;

    private static final String PATH = "./src/data/sky/";
    private static final String EXT = ".txt";
    private static final String SEPARATOR = ";";

    private static final String TOP = "G";
    private static final String BOTTOM = "D";
    private static final String LEFT = "L";
    private static final String RIGHT = "P";

    public SkyscrapperProblem(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void load() {
        reset();

        File file = new File(PATH + fileName + EXT);

        try (Scanner scanner = new Scanner(file)) {
            boardSize = scanner.nextInt();

            // create default board
            createBoard();
            generateBoardConstraints();

            // create Skyscrapper constraints
            while (scanner.hasNext()) {
                String line = scanner.next();

                String[] splitted = line.split(SEPARATOR);

                String side = splitted[0];
                int[] values = Arrays.stream(splitted).skip(1).mapToInt(Integer::parseInt).toArray();

                for (int i = 0; i < values.length; i++) {
                    if (values[i] != 0) {
                        List<Variable> variablesInConstraint = new ArrayList<>(boardSize);
                        Constraint newConstraint;

                        switch (side) {
                            case TOP:
                                for (int j = 0; j < boardSize; j++) {
                                    variablesInConstraint.add(variables.get(i + j * boardSize));
                                }
                                break;
                            case BOTTOM:
                                for (int j = boardSize - 1; j >= 0; j--) {
                                    variablesInConstraint.add(variables.get(i + j * boardSize));
                                }
                                break;
                            case LEFT:
                                for (int j = 0; j < boardSize; j++) {
                                    variablesInConstraint.add(variables.get(i * boardSize + j));
                                }
                                break;
                            case RIGHT:
                                for (int j = boardSize - 1; j >= 0; j--) {
                                    variablesInConstraint.add(variables.get(i * boardSize + j));
                                }
                                break;
                        }

                        newConstraint = new SkyscrapperConstraint(values[i], variablesInConstraint);
                        constraints.add(newConstraint);

                        for (Variable v :
                                variablesInConstraint) {
                            v.addConstraint(newConstraint);
                        }
                    }
                }
            }

            isLoaded = true;
        } catch (
                FileNotFoundException e) {
            isLoaded = false;
        }

    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public List<Variable> getVariables() {
        return variables;
    }

    @Override
    public Set<Constraint> getConstraints() {
        return constraints;
    }
}
