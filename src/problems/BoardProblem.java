package problems;

import constraints.BoardConstraint;
import constraints.Constraint;
import variables.Variable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

abstract class BoardProblem implements Problem {
    List<Variable> variables;
    Set<Constraint> constraints;

    String fileName;

    int boardSize;

    private List<String> results;

    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String OUT_PATH = "./src/out/";
    private static final String OUT_EXT = ".html";

    void createBoard() {
        variables = new ArrayList<>(boardSize * boardSize);
        constraints = new HashSet<>();

        // create domain (for not set variables
        List<Integer> values = new ArrayList<>(boardSize);

        for (int i = 1; i <= boardSize; i++) {
            values.add(i);
        }

        for (int row = 0; row < boardSize; row++) {
            String rowId = String.valueOf(ALPHABET.charAt(row));

            for (int column = 0; column < boardSize; column++) {
                String id = rowId + (column + 1);
                variables.add(new Variable(id, values));
            }
        }
    }

    void generateBoardConstraints() {
        for (int row = 0; row < boardSize; row++) {
            // create row constraints
            int finalRow = row;
            HashSet<Variable> vars = IntStream.range(0, boardSize * boardSize).filter(i -> i / boardSize == finalRow).mapToObj(n -> variables.get(n)).collect(Collectors.toCollection(HashSet::new));
            Constraint tmp = new BoardConstraint(vars);
            vars.forEach(v -> v.addConstraint(tmp));
            constraints.add(tmp);
        }

        for (int column = 0; column < boardSize; column++) {
            // generate column constraints
            int finalColumn = column;
            HashSet<Variable> vars = IntStream.range(0, boardSize * boardSize).filter(i -> i % boardSize == finalColumn).mapToObj(n -> variables.get(n)).collect(Collectors.toCollection(HashSet::new));
            Constraint tmp = new BoardConstraint(vars);
            vars.forEach(v -> v.addConstraint(tmp));
            constraints.add(tmp);
        }
    }

    @Override
    public String getName() {
        return fileName;
    }

    void reset() {
        if (variables != null) {
            variables.clear();
        }


        if (constraints != null) {
            constraints.clear();
        }
    }

    @Override
    public void storeCurrent() {
        if (results == null) {
            results = new ArrayList<>();
        }

        StringBuilder builder = new StringBuilder();

        builder.append("<table>");

        for (int i = 0; i < boardSize; i++) {
            builder.append("<tr>");

            for (int j = 0; j < boardSize; j++) {
                builder.append("<td>");
                builder.append(variables.get(i * boardSize + j).getValue());
                builder.append("</td>");
            }
            builder.append("</tr>");
        }
        builder.append("</table>");

        results.add(builder.toString());
    }

    @Override
    public void saveToFile(String prefix) {
        if (results == null) {
            results = new ArrayList<>();
        }

        File file = new File(OUT_PATH + prefix + fileName + OUT_EXT);

        try (PrintWriter pw = new PrintWriter(file)) {
            for (String solution :
                    results) {
                pw.println(solution);
                pw.println("<br>");
            }

            System.out.println("Saved to " + file.getPath());
        } catch (FileNotFoundException e) {
            System.err.println("Can't write to file");
        }
    }
}
