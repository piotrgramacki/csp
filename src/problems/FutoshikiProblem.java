package problems;

import constraints.Constraint;
import constraints.FutoshikiConstraint;
import variables.Variable;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FutoshikiProblem extends BoardProblem {
    private boolean isLoaded = false;

    private static final String PATH = "./src/data/futo/";
    private static final String EXT = ".txt";
    private static final String SEPARATOR = ";";

    public FutoshikiProblem(String fileName) {
        super();
        this.fileName = fileName;
    }

    @Override
    public void load() {
        reset();

        File file = new File(PATH + fileName + EXT);

        try (Scanner scanner = new Scanner(file)) {
            // load size
            boardSize = scanner.nextInt();

            // skip "START:"
            scanner.next();

            // create board variables
            createBoard();

            // set fixed values
            for (int i = 0; i < boardSize; i++) {
                // read one row
                String line = scanner.next();

                // split to cells
                String[] splited = line.split(SEPARATOR);

                for (int j = 0; j < splited.length; j++) {
                    String val = splited[j];

                    // check if variable should be set
                    if (Integer.parseInt(val) != 0) {
                        variables.get(i * boardSize + j).setFixed(Integer.parseInt(val));
                    }
                }
            }

            generateBoardConstraints();

            // skip REL: line
            scanner.next();

            // read futoshiki constraints
            while (scanner.hasNext()) {
                String line = scanner.next();

                String[] splitted = line.split(SEPARATOR);

                Variable smaller = variables.stream().filter(v -> v.getId().equals(splitted[0])).findAny().get();
                Variable bigger = variables.stream().filter(v -> v.getId().equals(splitted[1])).findAny().get();

                Constraint constraint = new FutoshikiConstraint(smaller, bigger);

                smaller.addConstraint(constraint);
                bigger.addConstraint(constraint);

                constraints.add(constraint);
            }

            isLoaded = true;
        } catch (FileNotFoundException e) {
            isLoaded = false;
        }
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public Set<Constraint> getConstraints() {
        return constraints;
    }
}
