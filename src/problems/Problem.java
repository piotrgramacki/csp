package problems;

import constraints.Constraint;
import variables.Variable;

import java.util.List;
import java.util.Set;

public interface Problem {
    void load();

    boolean isLoaded();

    List<Variable> getVariables();

    Set<Constraint> getConstraints();

    void storeCurrent();

    void saveToFile(String prefix);

    String getName();
}
