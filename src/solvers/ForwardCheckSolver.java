package solvers;

import problems.Problem;
import variables.Variable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ForwardCheckSolver implements Solver {
    private List<Variable> variables;

    private Problem problem;

    private int checkedValues;
    private int checkedToFirst;

    @Override
    public int getCheckedToFirst() {
        return checkedToFirst;
    }

    @Override
    public int solve(Problem problem) {
        checkedValues = 0;
        checkedToFirst = 0;

        if (!problem.isLoaded()) {
            problem.load();
        }

        if (problem.isLoaded()) {

            this.problem = problem;

            variables = new ArrayList<>(problem.getVariables());

            preprocessing();

            Optional<Variable> startPoint = chooseVariable();

            if (startPoint.isPresent()) {
                forwardCheckStep(startPoint.get());
            } else {
                problem.storeCurrent();
            }

            problem.saveToFile("FC_");
        } else {
            System.err.println("Can't load problem");
        }

        return checkedValues;
    }

    private void preprocessing() {
        variables.forEach(Variable::cutDomain);
    }

    private void forwardCheckStep(Variable current) {
        List<Variable> constrainedVariables = current.getConnectedVariables();

        current.storeDomain();

        // value heuristic here
//        current.sortDomain();

        while (current.nextValue()) {
            checkedValues++;

            constrainedVariables.forEach(Variable::cutDomain);

            if (constrainedVariables.stream().allMatch(Variable::hasNextValue)) {
                Optional<Variable> nextVariable = chooseVariable(current);

                if (nextVariable.isPresent()) {
                    forwardCheckStep(nextVariable.get());
                } else {
                    if (checkedToFirst == 0) {
                        checkedToFirst = checkedValues;
                    }
                    problem.storeCurrent();
                }
            }

            constrainedVariables.forEach(Variable::restoreDomain);
        }

        current.reset();
    }

    private Optional<Variable> chooseVariable() {
        return variables.stream()
                .filter(Variable::isNull)
                // switch between .findFirst() and .min to activate/deactivate variable choice heuristic
                .findFirst();
//                .min((o1, o2) -> {
//                    int value = o1.getAvailableDomainSize() - o2.getAvailableDomainSize();
//
//                    if (value == 0) {
//                        value = o2.getConstraintsNumber() - o1.getConstraintsNumber();
//                    }
//
//                    return value;
//                });
    }

    private Optional<Variable> chooseVariable(Variable current) {
        return variables.stream()
                .filter(variable -> variable.isNullAndDifferent(current))
                // switch between .findFirst() and .min to activate/deactivate variable choice heuristic
                .findFirst();
//                .min((o1, o2) -> {
//                    int value = o1.getAvailableDomainSize() - o2.getAvailableDomainSize();
//
//                    if (value == 0) {
//                        value = o2.getConstraintsNumber() - o1.getConstraintsNumber();
//                    }
//
//                    return value;
//                });
    }
}
