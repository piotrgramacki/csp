package solvers;

import problems.Problem;

public interface Solver {
    int getCheckedToFirst();

    int solve(Problem problem);
}
