package solvers;

import constraints.Constraint;
import problems.Problem;
import variables.Variable;

import java.util.*;

public class BacktrackSolver implements Solver {
    private List<Variable> variables;
    private Set<Constraint> constraints;

    private Problem problem;

    private int checkedValues;
    private int checkedToFirst = 0;

    @Override
    public int getCheckedToFirst() {
        return checkedToFirst;
    }

    @Override
    public int solve(Problem problem) {
        checkedValues = 0;
        checkedToFirst = 0;

        if (!problem.isLoaded()) {
            problem.load();
        }

        if (problem.isLoaded()) {

            this.problem = problem;

            variables = new ArrayList<>(problem.getVariables());

            // Most Constrained Variable heuristic application
//            variables.sort((v1, v2) -> Integer.compare(v2.getConstraintsNumber(), v1.getConstraintsNumber()));

            constraints = problem.getConstraints();

            Optional<Variable> startPoint = chooseVariable();

            if (startPoint.isPresent()) {
                backtrackStep(startPoint.get());
            } else {
                problem.storeCurrent();
            }

            problem.saveToFile("BT_");
        } else {
            System.err.println("Can't load problem");
        }

        return checkedValues;
    }


    private void backtrackStep(Variable current) {
        // value heuristic
//        current.sortDomain();

        while (current.nextValue()) {
            checkedValues++;
            if (current.check()) {
                Optional<Variable> nextVariable = chooseVariable(current);
                if (nextVariable.isPresent()) {
                    backtrackStep(nextVariable.get());
                } else {
                    if (checkedToFirst == 0) {
                        checkedToFirst = checkedValues;
                    }
                    problem.storeCurrent();
                }
            }
        }
        current.reset();
    }

    private Optional<Variable> chooseVariable() {
        return variables.stream()
                .filter(Variable::isNull)
                .findFirst();
    }

    private Optional<Variable> chooseVariable(Variable current) {
        return variables.stream()
                .filter(variable -> variable.isNullAndDifferent(current))
                .findFirst();
    }
}
